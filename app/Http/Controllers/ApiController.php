<?php

namespace App\Http\Controllers;

use App\LogUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ApiController extends Controller
{

   /* public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('client');
    }*/

    public function api(Request $request)
    {

        $userName = $request->input('adminid');

        $log = new LogUser();

        $log->set_table('log_'.$userName);
        $ipDetails = $log->where('adminid', $userName)->where('ip', '!=', 0)->where('signature', 0)
            ->orWhere('adminid', $userName)->where('signature', 2)
            ->orWhere('adminid', $userName)->where('status', 4)
            ->orWhere('adminid', $userName)->where('status', 6)
            ->get();

        //dd($ipDetails);
        //$ipList = array();
        foreach ($ipDetails as $ipDetail) {

            if ($ipDetail->signature == 0 && $ipDetail->ip != 0) {
                $ipDetail->signature = 1;

                //return view('api', compact('ipDetail'));
            }

            if ($ipDetail->signature == 2 && $ipDetail->del_ip != 0) {
                $ipDetail->signature = 3;
            }

            if ($ipDetail->status == 4 && $ipDetail->sip_ip != 0) {
                $ipDetail->status = 5;
            }

            if ($ipDetail->status == 6 && $ipDetail->del_sip_ip != 0) {
                $ipDetail->status = 7;
            }
            $ipDetail->save();

        }

        echo $allIPs = json_encode($ipDetails);
        //$array = json_decode($allIPs, true);

       // dd($array);


    }
}
