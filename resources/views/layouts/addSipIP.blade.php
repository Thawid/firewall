@extends('layouts.master')

@section('title',"CloudCoder || Firewall")
@section('style')
    {{-- <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">--}}
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add SIP IP</h3>
        </div>
        <div class="box-header with-border">


            @if(session()->has('status'))
                <p class="alert alert-info">
                    {{  session()->get('status') }}
                </p>
            @endif
            <div class="col-sm-6">
                <div class="panel panel-default">

                    <div class="panel-body">
                        {{ Form::open(['url' => route('storesipip'), 'method' => 'POST' ]) }}

                        <div class="form-group">
                            {{ Form::label('name', 'SIP IP') }} <em>*</em>
                            {{ Form::text('sip_ip', null, ['class' => 'form-control', 'id' => 'sip_ip', 'required' => 'required', 'placeholder' => 'Ex : 127.0.0.0, 127.0.0.1']) }}

                        </div>



                        <div class="form-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection


@section('script')

    <script> console.log('Hi!'); </script>

@endsection
