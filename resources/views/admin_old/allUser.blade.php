
<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 11:58 AM
 */
?>

@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

    <h1>Dashboard</h1>

@stop



@section('content')

    <div class="col-sm-12">

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                User Listing
                <a href="{{ route('admin.create') }}" class="btn btn-success btn-xs">Add User</a>
            </div>
            <div class="panel-body">
                @if (count($users))
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Created On</th>
                                <th>Last Updated</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->format('m-d-Y') }}</td>
                                    <td>{{ $user->updated_at->format('m-d-Y') }}</td>
                                    <td>
                                        <a href="{{ route('admin.update', $user->id) }}" class="btn btn-success btn-xs">Edit</a>
                                        <a href="{{ route('admin.view', $user->id) }}" class="btn btn-info btn-xs">View</a>
                                        <form action="{{ route('admin.destroy', $user->id) }}" method="POST" style="display:inline-block">
                                            {{ csrf_field() }}
                                            <button class="btn btn-danger btn-xs">
                                                <span>DELETE</span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        {{ $users->links() }}
                    </div>
                @else
                    <p class="alert alert-info">
                        No Listing Found
                    </p>
                @endif
            </div>
        </div>
    </div>
    </div>

@stop



@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop



@section('js')

    <script> console.log('Hi!'); </script>

@stop

