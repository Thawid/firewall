@extends('layouts.master')

@section('title',"CloudCoder || Firewall")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Client Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')
    @if (isset($clientIP))

        <p>Welcome to this beautiful client panel - </p>



        <p class="result"> Your IP Is : {{$clientIP}} , Please Wait For Sync..</p>



    @endif
    @if (isset($syncClientIP))

        <p>Welcome to this beautiful client panel - </p>
        <p class="result"> Your IP Is : {{$syncClientIP}} , is successfully Synced</p>


    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Last Five Login IP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover table-bordered" id="table">
                <thead>
                <th> SL</th>
                <th> IP</th>
                <th> Login Date</th>


                </thead>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop



@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script> console.log('Hi!'); </script>
    <script>
        window.setInterval(function () {
            $.ajax({
                url: '{{route('getIPStatus',['adminid'=>$user_id])}}',
                context: document.body
            }).done(function (data) {
                $(".result").html(data);
            });
        }, 5000);
    </script>
    <script>
        $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '{{route('homeLogDetails')}}',
                "type": "get"
            },
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            },
        });
    </script>
@endsection
