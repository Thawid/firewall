<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>->
            <!-- Optionally, you can add icons to the links -->
            <li  class="active">
                <a href="{{url('admin/home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>
            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="glyphicon glyphicon-user"></i>
                    <span>Client</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/allUser')) class="active" @endif><a
                                href="{{url('admin/allUser')}}"><i class="fa fa-circle-o"></i>All Clients</a>
                    </li>
                    <li @if(URL::current()==url('admin.create')) class="active" @endif><a
                                href="{{url('admin/create')}}"><i class="fa fa-circle-o"></i> New Client</a>
                    </li>

                </ul>
            </li>
            <li @if(URL::current()==url('admin/addVersion')) class="active" @endif><a
                        href="{{url('admin/addVersion')}}"> <i class="fa fa-fw fa-plus-square"></i>  <span> Version </span></a>
            </li>
            <li>
                <a href="{{url('admin/logHistory')}}">
                    <i class="fa fa-object-ungroup" aria-hidden="true"></i> <span>History</span>
                </a>
            </li>
            <li>
                <a href="{{url('logout')}}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> <span>Sign Out</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
