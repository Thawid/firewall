<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 1:44 PM
 */
?>

@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

    <h1>Dashboard</h1>

@stop



@section('content')
    <section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Client</h3>
        </div>
        <div class="box-header with-border">


            @if(session()->has('status'))
                <p class="alert alert-info">
                    {{  session()->get('status') }}
                </p>
            @endif
            <div class="col-sm-6">
                <div class="panel panel-default">

                    <div class="panel-body">
                        {{ Form::open(['url' => route('users.store'), 'method' => 'POST' ]) }}
                        @include('admin.users._form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>


    </div>
    </section>
@stop



@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop



@section('js')

    <script> console.log('Hi!'); </script>

@stop

