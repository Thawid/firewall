
@extends('admin.layout.master')

@section('title',"CloudCoder || Firewall")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')

    <div class="col-sm-12">

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                Client List
                <a style="margin-left: 776px;" href="{{ route('admin.create') }}" class="btn btn-primary btn-xs">Add Client</a>
            </div>
            <div class="panel-body">
                @if (count($users))
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Created On</th>
                                <th>Last Updated</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->format('m-d-Y') }}</td>
                                    <td>{{ $user->updated_at->format('m-d-Y') }}</td>
                                    <td>
                                        <a href="{{ route('admin.edit', $user->id) }}" class="btn btn-success btn-xs"><i class="fa fa-edit "></i></a>
                                        <a href="{{ route('admin.view', $user->id) }}" class="btn btn-info btn-xs"><i class="fa fa-eye "></i></a>
                                        <form action="{{ route('admin.destroy', $user->id) }}" method="POST" style="display:inline-block">
                                            {{ csrf_field() }}
                                            <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash "></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        {{ $users->links() }}
                    </div>
                @else
                    <p class="alert alert-info">
                        No Listing Found
                    </p>
                @endif
            </div>
        </div>
    </div>
    </div>

@stop



@section('script')
    <script> console.log('Hi!'); </script>
@endsection

