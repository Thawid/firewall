
@extends('admin.layout.master')

@section('title',"CloudCoder || Firewall")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection


@section('content')
    <section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Client</h3>
        </div>
        <div class="box-header with-border">


            @if(session()->has('status'))
                <p class="alert alert-info">
                    {{  session()->get('status') }}
                </p>
            @endif
            <div class="col-sm-6">
                <div class="panel panel-default">

                    <div class="panel-body">
                        {{ Form::open(['url' => route('admin.store'), 'method' => 'POST' ]) }}
                        @include('admin._form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>


    </div>
    </section>
@stop




@section('script')
    <script> console.log('Hi!'); </script>
@endsection

